﻿using System;
using System.ComponentModel;
using System.IO;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using Microsoft.Kinect;

namespace ADR
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow
    {
        private const float RenderWidth = 800.0f;//640.0f;
        private const float RenderHeight = 600.0f;//480.0f;
        private const double JointThickness = 3;
        private const double BodyCenterThickness = 10;
        private const double ClipBoundsThickness = 10;
        private readonly Brush _centerPointBrush = Brushes.Red;
        private readonly Brush _trackedJointBrush = new SolidColorBrush(Color.FromArgb(255, 68, 192, 68));
        private readonly Brush _inferredJointBrush = Brushes.Blue;
        private readonly Pen _trackedBonePen = new Pen(Brushes.Green, 6);
        private readonly Pen _inferredBonePen = new Pen(Brushes.Gray, 1);
        private KinectSensor _kinect;
        private DrawingGroup _drawingGroup;
        private DrawingImage _imageSource;
        private static readonly int Bgr32BytesPerPixel = (PixelFormats.Bgr32.BitsPerPixel + 7) / 8;
        private byte[] _pixelData;
        private WriteableBitmap _cameraSource;

        public MainWindow()
        {
            InitializeComponent();
        }

        private void WindowLoaded(object sender, RoutedEventArgs e)
        {
            _drawingGroup = new DrawingGroup();
            _imageSource = new DrawingImage(_drawingGroup);
            Image.Source = _imageSource;
            MessageBox.Show("Number of Kinect Sensor(s) Connected : " + KinectSensor.KinectSensors.Count);

            foreach (var potentialSensor in KinectSensor.KinectSensors)
            {
                if (potentialSensor.Status != KinectStatus.Connected) continue;
                _kinect = potentialSensor;
                break;
            }

            if (_kinect != null)
            {
                _kinect.SkeletonStream.Enable();
                _kinect.SkeletonFrameReady += SensorSkeletonFrameReady;

                _kinect.ColorStream.Enable();
                _kinect.ColorFrameReady += (ColorImageFrameReady_handler);
                _kinect.ColorFrameReady += ColorImageReady;
            }

            // Start the sensor!
            try
            {
                _kinect?.Start();
            }
            catch (IOException)
            {
                _kinect = null;
            }
        }

        private void WindowClosing(object sender, CancelEventArgs e)
        {
            _kinect?.Stop();
        }

        private void ColorImageFrameReady_handler(object sender, ColorImageFrameReadyEventArgs e)
        {
            Camera.Source = _cameraSource;
        }

        private void ColorImageReady(object sender, ColorImageFrameReadyEventArgs e)
        {
            using (ColorImageFrame imageFrame = e.OpenColorImageFrame())
            {
                if (imageFrame == null) return;
                _cameraSource = new WriteableBitmap(imageFrame.Width, imageFrame.Height, 96, 96, PixelFormats.Bgr32, null);

                if (_pixelData == null)
                {
                    _pixelData = new byte[imageFrame.PixelDataLength];
                }

                imageFrame.CopyPixelDataTo(_pixelData);

                // A WriteableBitmap is a WPF construct that enables resetting the Bits of the image.
                // This is more efficient than creating a new Bitmap every frame.
                _cameraSource.WritePixels(
                    new Int32Rect(0, 0, imageFrame.Width, imageFrame.Height),
                    _pixelData,
                    imageFrame.Width * Bgr32BytesPerPixel,
                    0
                );
            }
        }

        private void SensorSkeletonFrameReady(object sender, SkeletonFrameReadyEventArgs e)
        {
            Skeleton[] skeletons;

            using (var skeletonFrame = e.OpenSkeletonFrame())
            {
                if (skeletonFrame == null) return;
                skeletons = new Skeleton[skeletonFrame.SkeletonArrayLength];
                skeletonFrame.CopySkeletonDataTo(skeletons);
            }

            using (var dc = _drawingGroup.Open())

            {
                dc.DrawRectangle(Brushes.Transparent, null, new Rect(0.0, 0.0, RenderWidth, RenderHeight));

                if (skeletons.Length == 0) return;
                foreach (var skeleton in skeletons)
                {
                    //RenderClippedEdges(skeleton, dc);

                    switch (skeleton.TrackingState)
                    {
                        case SkeletonTrackingState.Tracked:
                            DrawBonesAndJoints(skeleton, dc);
                            break;
                        case SkeletonTrackingState.PositionOnly:
                            dc.DrawEllipse(
                                _centerPointBrush,
                                null,
                                SkeletonPointToScreen(skeleton.Position),
                                BodyCenterThickness,
                                BodyCenterThickness
                            );
                            break;
                        case SkeletonTrackingState.NotTracked:
                            break;
                        default:
                            throw new ArgumentOutOfRangeException();
                    }
                }

                _drawingGroup.ClipGeometry = new RectangleGeometry(
                    new Rect(0.0, 0.0, RenderWidth, RenderHeight)
                );
            }
        }

        private void DrawBonesAndJoints(Skeleton skeleton, DrawingContext dc)
        {
            // Render Torso
            DrawBone(skeleton, dc, JointType.Head, JointType.ShoulderCenter);
            DrawBone(skeleton, dc, JointType.ShoulderCenter, JointType.ShoulderLeft);
            DrawBone(skeleton, dc, JointType.ShoulderCenter, JointType.ShoulderRight);
            DrawBone(skeleton, dc, JointType.ShoulderCenter, JointType.Spine);
            DrawBone(skeleton, dc, JointType.Spine, JointType.HipCenter);
            DrawBone(skeleton, dc, JointType.HipCenter, JointType.HipLeft);
            DrawBone(skeleton, dc, JointType.HipCenter, JointType.HipRight);

            // Left Arm
            DrawBone(skeleton, dc, JointType.ShoulderLeft, JointType.ElbowLeft);
            DrawBone(skeleton, dc, JointType.ElbowLeft, JointType.WristLeft);
            DrawBone(skeleton, dc, JointType.WristLeft, JointType.HandLeft);

            // Right Arm
            DrawBone(skeleton, dc, JointType.ShoulderRight, JointType.ElbowRight);
            DrawBone(skeleton, dc, JointType.ElbowRight, JointType.WristRight);
            DrawBone(skeleton, dc, JointType.WristRight, JointType.HandRight);

            // Left Leg
            DrawBone(skeleton, dc, JointType.HipLeft, JointType.KneeLeft);
            DrawBone(skeleton, dc, JointType.KneeLeft, JointType.AnkleLeft);
            DrawBone(skeleton, dc, JointType.AnkleLeft, JointType.FootLeft);

            // Right Leg
            DrawBone(skeleton, dc, JointType.HipRight, JointType.KneeRight);
            DrawBone(skeleton, dc, JointType.KneeRight, JointType.AnkleRight);
            DrawBone(skeleton, dc, JointType.AnkleRight, JointType.FootRight);

            // Render Joints
            foreach (Joint joint in skeleton.Joints)
            {
                Brush drawBrush = null;

                switch (joint.TrackingState)
                {
                    case JointTrackingState.Tracked:
                        drawBrush = _trackedJointBrush;
                        break;
                    case JointTrackingState.Inferred:
                        drawBrush = _inferredJointBrush;
                        break;
                    case JointTrackingState.NotTracked:
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }

                if (drawBrush != null)
                {
                    dc.DrawEllipse(drawBrush, null, SkeletonPointToScreen(joint.Position), JointThickness, JointThickness);
                }
            }
        }

        private Point SkeletonPointToScreen(SkeletonPoint skeletonPosition)
        {
            var depthPoint =
                _kinect.CoordinateMapper.MapSkeletonPointToDepthPoint(skeletonPosition,
                    DepthImageFormat.Resolution640x480Fps30);
            return new Point(depthPoint.X, depthPoint.Y);
        }

        private void DrawBone(Skeleton skeleton, DrawingContext dc, JointType firstJointType, JointType lastJointType)
        {
            var headJoint = skeleton.Joints[firstJointType];
            var shoulderJoint = skeleton.Joints[lastJointType];

            if (headJoint.TrackingState == JointTrackingState.NotTracked || shoulderJoint.TrackingState == JointTrackingState.NotTracked) return;
            
            if (headJoint.TrackingState == JointTrackingState.Inferred || shoulderJoint.TrackingState == JointTrackingState.NotTracked) return;

            var drawPen = _inferredBonePen;
            if (headJoint.TrackingState == JointTrackingState.Tracked &&
                shoulderJoint.TrackingState == JointTrackingState.Tracked) drawPen = _trackedBonePen;

            dc.DrawLine(drawPen, SkeletonPointToScreen(headJoint.Position), SkeletonPointToScreen(shoulderJoint.Position));
        }

        private void CheckBoxSeatedModeChanged(object sender, RoutedEventArgs e)
        {
            if (_kinect == null) return;
            _kinect.SkeletonStream.TrackingMode = 
                CheckBoxSeatedMode.IsChecked.GetValueOrDefault() ? 
                    SkeletonTrackingMode.Seated : SkeletonTrackingMode.Default;
        }
    }
}
